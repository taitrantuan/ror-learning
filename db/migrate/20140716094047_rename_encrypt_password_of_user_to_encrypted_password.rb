class RenameEncryptPasswordOfUserToEncryptedPassword < ActiveRecord::Migration
  def change
    rename_column :users, :encrypt_password, :encrypted_password
  end
end
