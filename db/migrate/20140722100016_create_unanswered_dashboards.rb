class CreateUnansweredDashboards < ActiveRecord::Migration
  def change
    create_table :unanswered_dashboards do |t|

      t.timestamps
    end
  end
end
