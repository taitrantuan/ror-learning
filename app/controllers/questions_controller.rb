class QuestionsController < ApplicationController
  def index
    # TODO: sort questions of current user to the top
    @questions = Question.all
  end

  def ask
    @question = Question.new
  end

  def create
    @question = Question.new question_params
    respond_to do |format|
      if @question.save
        format.html { redirect_to questions_path }
      else
        format.html { render :ask }
      end
    end
  end

  def question_params
    params.require(:question).permit(:title, :content, :tags_text)
  end
end
