class QuestionDashboardsController < ApplicationController
  before_action :set_question_dashboard, only: [:show, :edit, :update, :destroy]

  # GET /question_dashboards
  # GET /question_dashboards.json
  def index
    @question_dashboards = QuestionDashboard.all
  end

  # GET /question_dashboards/1
  # GET /question_dashboards/1.json
  def show
  end

  # GET /question_dashboards/new
  def new
    @question_dashboard = QuestionDashboard.new
  end

  # GET /question_dashboards/1/edit
  def edit
  end

  # POST /question_dashboards
  # POST /question_dashboards.json
  def create
    @question_dashboard = QuestionDashboard.new(question_dashboard_params)

    respond_to do |format|
      if @question_dashboard.save
        format.html { redirect_to @question_dashboard, notice: 'Question dashboard was successfully created.' }
        format.json { render :show, status: :created, location: @question_dashboard }
      else
        format.html { render :new }
        format.json { render json: @question_dashboard.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /question_dashboards/1
  # PATCH/PUT /question_dashboards/1.json
  def update
    respond_to do |format|
      if @question_dashboard.update(question_dashboard_params)
        format.html { redirect_to @question_dashboard, notice: 'Question dashboard was successfully updated.' }
        format.json { render :show, status: :ok, location: @question_dashboard }
      else
        format.html { render :edit }
        format.json { render json: @question_dashboard.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /question_dashboards/1
  # DELETE /question_dashboards/1.json
  def destroy
    @question_dashboard.destroy
    respond_to do |format|
      format.html { redirect_to question_dashboards_url, notice: 'Question dashboard was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question_dashboard
      @question_dashboard = QuestionDashboard.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_dashboard_params
      params[:question_dashboard]
    end
end
