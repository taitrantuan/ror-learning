class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.authenticate(user_params[:email], user_params[:password])
    if user.nil?
      flash.now[:error] = 'Incorrect email or password.'
      render 'new'
    else
      sign_in user
      redirect_to root_path
    end
  end

  def destroy
    if current_user
      sign_out
      redirect_to root_path
    else

    end
  end

  private
    def user_params
      params.require(:session).permit(:email, :password)
    end

end
