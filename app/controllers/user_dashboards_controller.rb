class UserDashboardsController < ApplicationController
  before_action :set_user_dashboard, only: [:show, :edit, :update, :destroy]

  # GET /user_dashboards
  # GET /user_dashboards.json
  def index
    @user_dashboards = UserDashboard.all
  end

  # GET /user_dashboards/1
  # GET /user_dashboards/1.json
  def show
  end

  # GET /user_dashboards/new
  def new
    @user_dashboard = UserDashboard.new
  end

  # GET /user_dashboards/1/edit
  def edit
  end

  # POST /user_dashboards
  # POST /user_dashboards.json
  def create
    @user_dashboard = UserDashboard.new(user_dashboard_params)

    respond_to do |format|
      if @user_dashboard.save
        format.html { redirect_to @user_dashboard, notice: 'User dashboard was successfully created.' }
        format.json { render :show, status: :created, location: @user_dashboard }
      else
        format.html { render :new }
        format.json { render json: @user_dashboard.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user_dashboards/1
  # PATCH/PUT /user_dashboards/1.json
  def update
    respond_to do |format|
      if @user_dashboard.update(user_dashboard_params)
        format.html { redirect_to @user_dashboard, notice: 'User dashboard was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_dashboard }
      else
        format.html { render :edit }
        format.json { render json: @user_dashboard.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_dashboards/1
  # DELETE /user_dashboards/1.json
  def destroy
    @user_dashboard.destroy
    respond_to do |format|
      format.html { redirect_to user_dashboards_url, notice: 'User dashboard was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_dashboard
      @user_dashboard = UserDashboard.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_dashboard_params
      params[:user_dashboard]
    end
end
