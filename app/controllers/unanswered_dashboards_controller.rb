class UnansweredDashboardsController < ApplicationController
  before_action :set_unanswered_dashboard, only: [:show, :edit, :update, :destroy]

  # GET /unanswered_dashboards
  # GET /unanswered_dashboards.json
  def index
    @unanswered_dashboards = UnansweredDashboard.all
  end

  # GET /unanswered_dashboards/1
  # GET /unanswered_dashboards/1.json
  def show
  end

  # GET /unanswered_dashboards/new
  def new
    @unanswered_dashboard = UnansweredDashboard.new
  end

  # GET /unanswered_dashboards/1/edit
  def edit
  end

  # POST /unanswered_dashboards
  # POST /unanswered_dashboards.json
  def create
    @unanswered_dashboard = UnansweredDashboard.new(unanswered_dashboard_params)

    respond_to do |format|
      if @unanswered_dashboard.save
        format.html { redirect_to @unanswered_dashboard, notice: 'Unanswered dashboard was successfully created.' }
        format.json { render :show, status: :created, location: @unanswered_dashboard }
      else
        format.html { render :new }
        format.json { render json: @unanswered_dashboard.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /unanswered_dashboards/1
  # PATCH/PUT /unanswered_dashboards/1.json
  def update
    respond_to do |format|
      if @unanswered_dashboard.update(unanswered_dashboard_params)
        format.html { redirect_to @unanswered_dashboard, notice: 'Unanswered dashboard was successfully updated.' }
        format.json { render :show, status: :ok, location: @unanswered_dashboard }
      else
        format.html { render :edit }
        format.json { render json: @unanswered_dashboard.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /unanswered_dashboards/1
  # DELETE /unanswered_dashboards/1.json
  def destroy
    @unanswered_dashboard.destroy
    respond_to do |format|
      format.html { redirect_to unanswered_dashboards_url, notice: 'Unanswered dashboard was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_unanswered_dashboard
      @unanswered_dashboard = UnansweredDashboard.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def unanswered_dashboard_params
      params[:unanswered_dashboard]
    end
end
