json.array!(@user_dashboards) do |user_dashboard|
  json.extract! user_dashboard, :id
  json.url user_dashboard_url(user_dashboard, format: :json)
end
