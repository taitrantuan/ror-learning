json.array!(@question_dashboards) do |question_dashboard|
  json.extract! question_dashboard, :id
  json.url question_dashboard_url(question_dashboard, format: :json)
end
