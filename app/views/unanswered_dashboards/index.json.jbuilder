json.array!(@unanswered_dashboards) do |unanswered_dashboard|
  json.extract! unanswered_dashboard, :id
  json.url unanswered_dashboard_url(unanswered_dashboard, format: :json)
end
