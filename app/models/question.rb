class Question < Post
  attr_accessor :tags_text

  validates :title, :presence => true
  validates :content, :presence => true
  validates :tags_text, :presence => true


  has_many :tags

end
