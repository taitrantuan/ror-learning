class User < ActiveRecord::Base
  attr_accessor :password

  # TODO: add validate for email and length validation for password
  # TODO: better validation show -> maybe use Angularjs
  # TODO: apply reCATCHA
  # TODO: add uniqueness constraint to db
  validates :user_name, :presence => true, :uniqueness => true
  validates :email, :presence => true, :uniqueness => true
  validates :password, :presence => true,
            :confirmation => true, :length => {:minimum => 6}
  validates :password_confirmation, :presence => true

  before_save :encrypt_password

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user.nil? || !user.password_equals?(password)
      nil
    else
      user
    end
  end

  def password_equals?(password)
    encrypted_password == encrypt(password)
  end


  private
  def encrypt_password
    if encrypted_password.blank?
      self.encrypted_password = encrypt(self.password)
    end
  end

  def encrypt(rawPassword)
    Digest::SHA2.hexdigest(rawPassword)
  end
end
