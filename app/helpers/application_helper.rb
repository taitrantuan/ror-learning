module ApplicationHelper
  def full_title(page_title)
    base_title = 'HealthQA'
    if page_title.empty?
      base_title
    else
      page_title + ' - ' + base_title
    end
  end

  def header_tag(name, path, controller_class, image_class = nil)
    if image_class
      link = link_to(path) do
        "<span class='glyphicon #{image_class}'></span>".html_safe + ' ' + name
      end
    else
      link = link_to name, path
    end

    if @_controller.instance_of?(controller_class)
      content_tag(:li, link, class: 'active')
    else
      content_tag(:li, link)
    end
  end
end
