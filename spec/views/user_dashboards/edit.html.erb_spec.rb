require 'rails_helper'

RSpec.describe "user_dashboards/edit", :type => :view do
  before(:each) do
    @user_dashboard = assign(:user_dashboard, UserDashboard.create!())
  end

  it "renders the edit user_dashboard form" do
    render

    assert_select "form[action=?][method=?]", user_dashboard_path(@user_dashboard), "post" do
    end
  end
end
