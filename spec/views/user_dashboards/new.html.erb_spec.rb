require 'rails_helper'

RSpec.describe "user_dashboards/new", :type => :view do
  before(:each) do
    assign(:user_dashboard, UserDashboard.new())
  end

  it "renders new user_dashboard form" do
    render

    assert_select "form[action=?][method=?]", user_dashboards_path, "post" do
    end
  end
end
