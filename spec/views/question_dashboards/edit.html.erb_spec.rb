require 'rails_helper'

RSpec.describe "question_dashboards/edit", :type => :view do
  before(:each) do
    @question_dashboard = assign(:question_dashboard, QuestionDashboard.create!())
  end

  it "renders the edit question_dashboard form" do
    render

    assert_select "form[action=?][method=?]", question_dashboard_path(@question_dashboard), "post" do
    end
  end
end
