require 'rails_helper'

RSpec.describe "question_dashboards/new", :type => :view do
  before(:each) do
    assign(:question_dashboard, QuestionDashboard.new())
  end

  it "renders new question_dashboard form" do
    render

    assert_select "form[action=?][method=?]", question_dashboards_path, "post" do
    end
  end
end
