require 'rails_helper'

RSpec.describe "unanswered_dashboards/new", :type => :view do
  before(:each) do
    assign(:unanswered_dashboard, UnansweredDashboard.new())
  end

  it "renders new unanswered_dashboard form" do
    render

    assert_select "form[action=?][method=?]", unanswered_dashboards_path, "post" do
    end
  end
end
