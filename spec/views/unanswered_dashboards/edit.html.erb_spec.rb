require 'rails_helper'

RSpec.describe "unanswered_dashboards/edit", :type => :view do
  before(:each) do
    @unanswered_dashboard = assign(:unanswered_dashboard, UnansweredDashboard.create!())
  end

  it "renders the edit unanswered_dashboard form" do
    render

    assert_select "form[action=?][method=?]", unanswered_dashboard_path(@unanswered_dashboard), "post" do
    end
  end
end
