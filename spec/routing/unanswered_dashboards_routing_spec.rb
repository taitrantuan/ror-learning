require "rails_helper"

RSpec.describe UnansweredDashboardsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/unanswered_dashboards").to route_to("unanswered_dashboards#index")
    end

    it "routes to #new" do
      expect(:get => "/unanswered_dashboards/new").to route_to("unanswered_dashboards#new")
    end

    it "routes to #show" do
      expect(:get => "/unanswered_dashboards/1").to route_to("unanswered_dashboards#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/unanswered_dashboards/1/edit").to route_to("unanswered_dashboards#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/unanswered_dashboards").to route_to("unanswered_dashboards#create")
    end

    it "routes to #update" do
      expect(:put => "/unanswered_dashboards/1").to route_to("unanswered_dashboards#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/unanswered_dashboards/1").to route_to("unanswered_dashboards#destroy", :id => "1")
    end

  end
end
