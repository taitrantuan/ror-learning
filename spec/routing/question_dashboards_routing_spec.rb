require "rails_helper"

RSpec.describe QuestionDashboardsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/question_dashboards").to route_to("question_dashboards#index")
    end

    it "routes to #new" do
      expect(:get => "/question_dashboards/new").to route_to("question_dashboards#new")
    end

    it "routes to #show" do
      expect(:get => "/question_dashboards/1").to route_to("question_dashboards#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/question_dashboards/1/edit").to route_to("question_dashboards#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/question_dashboards").to route_to("question_dashboards#create")
    end

    it "routes to #update" do
      expect(:put => "/question_dashboards/1").to route_to("question_dashboards#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/question_dashboards/1").to route_to("question_dashboards#destroy", :id => "1")
    end

  end
end
