require 'rails_helper'

RSpec.describe "Users", :type => :request do
  describe "User index page" do
    it "works! (now write some real specs)" do
      get users_path
      expect(response.status).to be(200)
    end
  end

  describe "creation user page - users/new" do
    it "title should be 'Signup - HealthQA'" do
      visit new_user_path
      page.should have_title('Signup - HealthQA')
    end
  end

  describe 'Signup' do
    describe 'failure' do
      it 'should not make new user' do
        lambda do
          visit signup_path
          click_button
          page.should render_template('new/user')
          page.should have_selector('h1', 'div#error_explanation')
        end.should_not change(User, :count)
      end
    end
  end
end
