require 'rails_helper'

describe 'User' do

  describe "basic validations" do
    before { @user = User.new(user_name: "test", email: "testing@gmail", password: "foo") }
    subject { @user }

    it { should respond_to(:user_name) }
    it { should respond_to(:email) }
    it { should respond_to(:password) }

    describe "when user_name is not present" do
      before { @user.user_name = '' }
      it { should_not be_valid }
    end

    describe "when user_name is present" do
      before { @user.user_name = 'testing' }
      it { should be_valid }
    end
  end

  describe 'password validations' do
    before(:each) {
      @attr = {
          :user_name => "Example user",
          :email => "example@mail.com",
          :password => "foo",
          :password_confirmation => "foo"
      }
    }

    it 'should require password' do
      User.new(@attr.merge(:password => '', :password_confirmation => '')).should_not be_valid
    end

    it 'should require a matching password confirmation' do
      User.new(@attr.merge(:password_confirmation => 'wrong password')).should_not be_valid
      User.new(@attr).should be_valid
    end

  end

  describe 'password_equals? ' do
    before(:each) {
      @user = User.create!(:user_name => 'Example user', :email => 'example@mail.com', :password => 'foo', :password_confirmation => 'foo')
    }

    it 'should return true if submitted_password and current user password are the same' do
      expect(@user.password_equals?('foo')).to be true
    end

    it 'should return false if submitted_password and current user password are not the same' do
      expect(@user.password_equals?('food')).to be false
    end

  end

end